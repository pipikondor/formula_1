package com.foxminded.formula1;

import com.foxminded.formula1.logic.RacingStage;
import com.foxminded.formula1.model.Racer;
import com.foxminded.formula1.view.RacingStageView;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        RacingStage firstStageRanking = new RacingStage();
        RacingStageView view = new RacingStageView();
        String abbreviationProperty = "abbreviations";
        String startProperty = "start";
        String endProperty = "end";
        List<Racer> racers = firstStageRanking.getRacers(abbreviationProperty, startProperty, endProperty);
        System.out.println(view.showRating(racers));
    }
}
