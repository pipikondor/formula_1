package com.foxminded.formula1.view;

import com.foxminded.formula1.model.Racer;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class RacingStageView {

    private final char SPACE = ' ';

    public String showRating(List<Racer> racers) {
        if (racers == null) {
            return "";
        }
        int nameColumnLength = getNameColumnLength(racers) + 2;
        int maxCarNameLength = getCarNameLength(racers);
        int timeLength = 9;
        int delimitersCount = 2;
        char lineSymbol = '-';
        StringBuilder view = new StringBuilder();
        racers.stream().limit(15).forEach(racer -> view.append(buildRacerLine(racer, nameColumnLength, maxCarNameLength)));
        if (racers.size() < 15) {
            return view.toString().trim();
        }
        int underLineLength = nameColumnLength + maxCarNameLength + timeLength + delimitersCount;
        view.append(buildLineFromSymbol(lineSymbol, underLineLength));
        view.append(System.lineSeparator());
        racers.stream().skip(15).forEach(racer -> view.append(buildRacerLine(racer, nameColumnLength, maxCarNameLength)));
        return view.toString().trim();
    }

    private int getNameColumnLength(List<Racer> racers) {
        return racers.stream().map(racer -> (racer.getPosition() + racer.getRacerName()).length()).max(Comparator.naturalOrder()).orElse(0);
    }

    private int getCarNameLength(List<Racer> racers) {
        return racers.stream().map(racer -> racer.getCarName().length()).max(Comparator.naturalOrder()).orElse(0);
    }

    private String buildRacerLine(Racer racer, int maxNameLength, int maxCarNameLength) {
        char delimiter = '|';
        StringBuilder racerLine = new StringBuilder();
        racerLine.append(buildNameColumn(racer, maxNameLength));
        racerLine.append(delimiter);
        racerLine.append(buildCarColumn(racer, maxCarNameLength));
        racerLine.append(delimiter);
        racerLine.append(buildTimeAsString(racer.getBestLap()));
        racerLine.append(System.lineSeparator());
        return racerLine.toString();
    }

    private String buildNameColumn(Racer racer, int maxNameColumnLength) {
        char point = '.';
        StringBuilder nameColumn = new StringBuilder();
        nameColumn.append(racer.getPosition());
        nameColumn.append(point);
        nameColumn.append(SPACE);
        nameColumn.append(racer.getRacerName());
        int nameOffset = maxNameColumnLength - nameColumn.length();
        nameColumn.append(buildLineFromSymbol(SPACE, nameOffset));
        return nameColumn.toString();
    }

    private String buildCarColumn(Racer racer, int maxCarNameLength) {
        StringBuilder carNameColumn = new StringBuilder(racer.getCarName());
        int carNameOffset = maxCarNameLength - carNameColumn.length();
        carNameColumn.append(buildLineFromSymbol(SPACE, carNameOffset));
        return carNameColumn.toString();
    }

    private String buildTimeAsString(Date time) {
        long timeAsLong = time.getTime();
        return String.format("%02d:%02d.%03d", TimeUnit.MILLISECONDS.toMinutes(timeAsLong),
            TimeUnit.MILLISECONDS.toSeconds(timeAsLong) % TimeUnit.HOURS.toMinutes(1),
            TimeUnit.MILLISECONDS.toMillis(timeAsLong) % TimeUnit.SECONDS.toMillis(1));
    }

    private String buildLineFromSymbol(char symbol, int lineOffset) {
        StringBuilder racerLine = new StringBuilder();
        for (int i = 0; i < lineOffset; i++) {
            racerLine.append(symbol);
        }
        return racerLine.toString();
    }
}
