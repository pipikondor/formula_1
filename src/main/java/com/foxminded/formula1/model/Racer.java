package com.foxminded.formula1.model;

import java.util.Date;
import java.util.Objects;

public class Racer implements Comparable<Racer> {

    private final String prefix;
    private final String carName;
    private final String racerName;
    private Date bestLap;
    private Date startTime;
    private Date endTime;
    private int position;

    public Racer(String prefix, String racerName, String carName) {
        this.prefix = prefix;
        this.carName = carName;
        this.racerName = racerName;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public void setBestLap(Date bestLap) {
        this.bestLap = bestLap;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public Date getStartTime() {
        return startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getCarName() {
        return carName;
    }

    public String getRacerName() {
        return racerName;
    }

    public Date getBestLap() {
        return bestLap;
    }

    public int getPosition() {
        return position;
    }

    @Override
    public int compareTo(Racer comparable) {
        if (this.bestLap.after(comparable.getBestLap())) {
            return 1;
        }
        if (this.bestLap.before(comparable.getBestLap())) {
            return -1;
        }
        return 0;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        Racer racer = (Racer) object;
        return position == racer.position && Objects.equals(prefix, racer.prefix) && Objects.equals(carName, racer.carName)
            && Objects.equals(racerName, racer.racerName) && Objects.equals(bestLap, racer.bestLap);
    }

    @Override
    public int hashCode() {
        return Objects.hash(prefix, carName, racerName, bestLap, position);
    }
}
