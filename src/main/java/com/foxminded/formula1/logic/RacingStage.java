package com.foxminded.formula1.logic;

import com.foxminded.formula1.model.Racer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class RacingStage {

    public List<Racer> getRacers(String abbreviationProperty, String startProperty, String endProperty) {
        ResourceLoader resourceLoader = new ResourceLoader();
        List<Racer> racers = resourceLoader.getFileAsStream(abbreviationProperty).map(racer -> {
            String[] racerAsArray = racer.split("_");
            return new Racer(racerAsArray[0], racerAsArray[1], racerAsArray[2]);
        }).collect(Collectors.toList());
        if (racers.isEmpty()) {
            return racers;
        }
        resourceLoader.getFileAsStream(startProperty).forEach(startTime -> findRacerByPrefix(racers, startTime.substring(0, 3))
            .setStartTime(getTimeFromString(startTime)));
        resourceLoader.getFileAsStream(endProperty).forEach(endTime -> {
            Racer racer = findRacerByPrefix(racers, endTime.substring(0, 3));
            racer.setEndTime(getTimeFromString(endTime));
            long lapTime = racer.getEndTime().getTime() - racer.getStartTime().getTime();
            if (lapTime < 0) {
                throw new IllegalArgumentException();
            }
            Date bestLap = new Date(lapTime);
            racer.setBestLap(bestLap);
        });
        return sortRacerList(racers);
    }

    private Racer findRacerByPrefix(List<Racer> racers, String prefix) {
        return racers.stream().filter(currentRacer -> currentRacer.getPrefix().equals(prefix))
            .findFirst().orElseThrow(NoSuchElementException::new);
    }

    private List<Racer> sortRacerList(List<Racer> racers) {
        List<Racer> sortedRacers = new ArrayList<>();
        AtomicInteger position = new AtomicInteger(1);
        racers.stream().sorted(Racer::compareTo).forEach(racer -> {
            racer.setPosition(position.getAndIncrement());
            sortedRacers.add(racer);
        });
        return sortedRacers;
    }

    private Date getTimeFromString(String timeWithPrefix) {
        Date dateTime;
        String time = timeWithPrefix.substring(4);
        SimpleDateFormat timeOne = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss.SSS");
        try {
            dateTime = timeOne.parse(time);
        } catch (ParseException e) {
            throw new IllegalArgumentException();
        }
        return dateTime;
    }
}
