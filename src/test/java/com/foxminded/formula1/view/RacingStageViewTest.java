package com.foxminded.formula1.view;

import com.foxminded.formula1.model.Racer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

class RacingStageViewTest {

    private RacingStageView racingStageView;
    private List<Racer> racers;
    private Racer racer;

    @BeforeEach
    void createFirstRacingStageView() {
        racingStageView = new RacingStageView();
        racers = new ArrayList<>();
    }

    @Test
    void showRating_shouldReturnEmptyString_whenInputIsNull() {
        Assertions.assertEquals("", racingStageView.showRating(null));
    }

    @Test
    void showRating_shouldReturnEmptyString_whenInputIsEmptyList() {
        Assertions.assertEquals("", racingStageView.showRating(racers));
    }

    @Test
    void showRating_shouldReturnViewWithoutUnderline_whenInputLengthLessThanFifteen() {
        racer = new Racer("DRR", "Daniel Ricciardo", "RED BULL RACING TAG HEUER");
        Date lapTime = new Date(60 * 1000 + 5 * 1000 + 894);
        racer.setBestLap(lapTime);
        racer.setPosition(1);
        racers.add(racer);
        StringBuilder exceptedView = new StringBuilder();
        exceptedView.append("1. Daniel Ricciardo|RED BULL RACING TAG HEUER|01:05.894");
        Assertions.assertEquals(exceptedView.toString(), racingStageView.showRating(racers));
    }

    @Test
    void showRating_shouldReturnTimeFormat_whenTimeLessThanMinute() {
        racer = new Racer("DRR", "Daniel Ricciardo", "RED BULL RACING TAG HEUER");
        Date lapTime = new Date(49 * 1000 + 576);
        racer.setBestLap(lapTime);
        racer.setPosition(1);
        racers.add(racer);
        StringBuilder exceptedView = new StringBuilder();
        exceptedView.append("1. Daniel Ricciardo|RED BULL RACING TAG HEUER|00:49.576");
        Assertions.assertEquals(exceptedView.toString(), racingStageView.showRating(racers));
    }

    @Test
    void showRating_shouldReturnViewWithUnderline_whenInputLengthGreaterThanFifteen() {
        racer = new Racer("DRR", "Daniel Ricciardo", "RED BULL RACING TAG HEUER");
        Date lapTime = new Date(60 * 1000 + 5 * 1000 + 894);
        racer.setBestLap(lapTime);
        racer.setPosition(1);
        racers.add(racer);
        racer = new Racer("VBM", "Valtteri Bottas", "MERCEDES");
        lapTime = new Date(60 * 1000 + 7 * 1000 + 453);
        racer.setBestLap(lapTime);
        racer.setPosition(2);
        racers.add(racer);
        racer = new Racer("EOF", "Esteban Ocon", "FORCE INDIA MERCEDES");
        lapTime = new Date(60 * 1000 + 8 * 1000 + 945);
        racer.setBestLap(lapTime);
        racer.setPosition(3);
        racers.add(racer);
        racer = new Racer("FAM", "Fernando Alonso", "MCLAREN RENAULT");
        lapTime = new Date(60 * 1000 + 8 * 1000 + 999);
        racer.setBestLap(lapTime);
        racer.setPosition(4);
        racers.add(racer);
        racer = new Racer("CSR", "Carlos Sainz", "RENAULT");
        lapTime = new Date(60 * 1000 + 9 * 1000 + 99);
        racer.setBestLap(lapTime);
        racer.setPosition(5);
        racers.add(racer);
        racer = new Racer("PGS", "Pierre Gasly", "SCUDERIA TORO ROSSO HONDA");
        lapTime = new Date(60 * 1000 + 9 * 1000 + 245);
        racer.setBestLap(lapTime);
        racer.setPosition(6);
        racers.add(racer);
        racer = new Racer("NHR", "Nico Hulkenberg", "RENAULT");
        lapTime = new Date(60 * 1000 + 9 * 1000 + 576);
        racer.setBestLap(lapTime);
        racer.setPosition(7);
        racers.add(racer);
        racer = new Racer("SVM", "Stoffel Vandoorne", "MCLAREN RENAULT");
        lapTime = new Date(60 * 1000 + 9 * 1000 + 895);
        racer.setBestLap(lapTime);
        racer.setPosition(8);
        racers.add(racer);
        racer = new Racer("SSW", "Sergey Sirotkin", "WILLIAMS MERCEDES");
        lapTime = new Date(60 * 1000 + 10 * 1000 + 45);
        racer.setBestLap(lapTime);
        racer.setPosition(9);
        racers.add(racer);
        racer = new Racer("CLS", "Charles Leclerc", "SAUBER FERRARI");
        lapTime = new Date(60 * 1000 + 10 * 1000 + 322);
        racer.setBestLap(lapTime);
        racer.setPosition(10);
        racers.add(racer);
        racer = new Racer("RGH", "Romain Grosjean", "HAAS FERRARI");
        lapTime = new Date(60 * 1000 + 10 * 1000 + 496);
        racer.setBestLap(lapTime);
        racer.setPosition(11);
        racers.add(racer);
        racer = new Racer("BHS", "Brendon Hartley", "SCUDERIA TORO ROSSO HONDA");
        lapTime = new Date(60 * 1000 + 11 * 1000 + 450);
        racer.setBestLap(lapTime);
        racer.setPosition(12);
        racers.add(racer);
        racer = new Racer("MES", "Marcus Ericsson", "SAUBER FERRARI");
        lapTime = new Date(60 * 1000 + 12 * 1000);
        racer.setBestLap(lapTime);
        racer.setPosition(13);
        racers.add(racer);
        racer = new Racer("LSW", "Lance Stroll", "WILLIAMS MERCEDES");
        lapTime = new Date(60 * 1000 + 14 * 1000 + 575);
        racer.setBestLap(lapTime);
        racer.setPosition(14);
        racers.add(racer);
        racer = new Racer("LSW", "Kevin Magnussen", "HAAS FERRARI");
        lapTime = new Date(60 * 1000 + 14 * 1000 + 896);
        racer.setBestLap(lapTime);
        racer.setPosition(15);
        racers.add(racer);
        racer = new Racer("NBB", "Nick Barkov", "BMW");
        lapTime = new Date(60 * 1000 + 14 * 1000 + 996);
        racer.setBestLap(lapTime);
        racer.setPosition(16);
        racers.add(racer);
        StringBuilder exceptedView = new StringBuilder();
        exceptedView.append("1. Daniel Ricciardo |RED BULL RACING TAG HEUER|01:05.894").append(System.lineSeparator());
        exceptedView.append("2. Valtteri Bottas  |MERCEDES                 |01:07.453").append(System.lineSeparator());
        exceptedView.append("3. Esteban Ocon     |FORCE INDIA MERCEDES     |01:08.945").append(System.lineSeparator());
        exceptedView.append("4. Fernando Alonso  |MCLAREN RENAULT          |01:08.999").append(System.lineSeparator());
        exceptedView.append("5. Carlos Sainz     |RENAULT                  |01:09.099").append(System.lineSeparator());
        exceptedView.append("6. Pierre Gasly     |SCUDERIA TORO ROSSO HONDA|01:09.245").append(System.lineSeparator());
        exceptedView.append("7. Nico Hulkenberg  |RENAULT                  |01:09.576").append(System.lineSeparator());
        exceptedView.append("8. Stoffel Vandoorne|MCLAREN RENAULT          |01:09.895").append(System.lineSeparator());
        exceptedView.append("9. Sergey Sirotkin  |WILLIAMS MERCEDES        |01:10.045").append(System.lineSeparator());
        exceptedView.append("10. Charles Leclerc |SAUBER FERRARI           |01:10.322").append(System.lineSeparator());
        exceptedView.append("11. Romain Grosjean |HAAS FERRARI             |01:10.496").append(System.lineSeparator());
        exceptedView.append("12. Brendon Hartley |SCUDERIA TORO ROSSO HONDA|01:11.450").append(System.lineSeparator());
        exceptedView.append("13. Marcus Ericsson |SAUBER FERRARI           |01:12.000").append(System.lineSeparator());
        exceptedView.append("14. Lance Stroll    |WILLIAMS MERCEDES        |01:14.575").append(System.lineSeparator());
        exceptedView.append("15. Kevin Magnussen |HAAS FERRARI             |01:14.896").append(System.lineSeparator());
        exceptedView.append("--------------------------------------------------------").append(System.lineSeparator());
        exceptedView.append("16. Nick Barkov     |BMW                      |01:14.996");
        Assertions.assertEquals(exceptedView.toString(), racingStageView.showRating(racers));
    }
}
