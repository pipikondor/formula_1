package com.foxminded.formula1.logic;

import com.foxminded.formula1.model.Racer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

class RacingStageTest {

    private RacingStage racingStage;

    @BeforeEach
    void createRacingStageObject() {
        racingStage = new RacingStage();
    }

    @Test
    void getRacers_shouldThrowNoSuchElementException_whenTimeNotFoundByAbbreviations() {
        Assertions.assertThrows(NoSuchElementException.class, () ->
            racingStage.getRacers("abbreviationsHasNotTime", "start", "end"));
    }

    @Test
    void getRacers_shouldThrowNoSuchElementException_whenAbbreviationsPrefixHasIncorrectFormat() {
        Assertions.assertThrows(NoSuchElementException.class, () ->
            racingStage.getRacers("abbreviationsInvalidFormat", "start", "end"));
    }

    @Test
    void getRacers_shouldThrowIndexOutBoundException_whenInputRacerStringHasNotThirdPart() {
        Assertions.assertThrows(IndexOutOfBoundsException.class, () ->
            racingStage.getRacers("abbreviationsHasNotThirdPart", "start", "end"));
    }

    @Test
    void getRacers_shouldReturnEmptyList_whenRacersIsEmpty() {
        List<Racer> racers = new ArrayList<>();
        Assertions.assertEquals(racers, racingStage.getRacers("abbreviationsEmpty", "start", "end"));
    }

    @Test
    void getRacers_shouldMatchRacerData_whenInputFormatCorrectAndDataExist() {
        Racer racer = new Racer("DRR", "Daniel Ricciardo", "RED BULL RACING TAG HEUER");
        Date lapTime = new Date(60 * 1000 + 12 * 1000 + 13);
        racer.setBestLap(lapTime);
        racer.setPosition(1);
        List<Racer> racers = new ArrayList<>();
        racers.add(racer);
        Assertions.assertEquals(racers, racingStage.getRacers("abbreviationsOneRacer", "startOneRacer", "endOneRacer"));
    }

    @Test
    void getRacers_shouldSortRacerData_whenInputHasMoreOneRacerData() {
        List<Racer> racers = new ArrayList<>();
        Racer racerFirst = new Racer("RGH", "Romain Grosjean", "HAAS FERRARI");
        Date lapTime = new Date(60 * 1000 + 12 * 1000 + 930);
        racerFirst.setBestLap(lapTime);
        racerFirst.setPosition(1);
        racers.add(racerFirst);
        Racer racerSecond = new Racer("MES", "Marcus Ericsson", "SAUBER FERRARI");
        Date lapTimeSecond = new Date(60 * 1000 + 13 * 1000 + 265);
        racerSecond.setPosition(2);
        racerSecond.setBestLap(lapTimeSecond);
        racers.add(racerSecond);
        Assertions.assertEquals(racers, racingStage.getRacers("abbreviations", "start", "end"));
    }

    @Test
    void getRacers_shouldThrowIllegalArgumentException_whenEndTimeLessThanStartTime() {
        Assertions.assertThrows(IllegalArgumentException.class, () ->
            racingStage.getRacers("abbreviationsOneRacer", "startOneRacer", "endLessThanStart"));
    }

    @Test
    void getRacers_shouldThrowIllegalArgumentException_whenTimeFormatIsIncorrect() {
        Assertions.assertThrows(IllegalArgumentException.class, () ->
            racingStage.getRacers("abbreviationsOneRacer", "startOneRacer", "endIncorrectFormat"));
    }
}
