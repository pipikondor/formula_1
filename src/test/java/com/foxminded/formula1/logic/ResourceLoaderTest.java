package com.foxminded.formula1.logic;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class ResourceLoaderTest {

    private ResourceLoader resourceLoader;

    @BeforeEach
    void createResourceLoader() {
        resourceLoader = new ResourceLoader();
    }

    @Test
    void getFileAsStream_shouldReturnEmptyStream_whenPropertyIsNotExist() {
        List<String> exceptedStreamAsList = Stream.of("").collect(Collectors.toList());
        Assertions.assertEquals(exceptedStreamAsList, resourceLoader.getFileAsStream("abc").collect(Collectors.toList()));
    }

    @Test
    void getFileAsStream_shouldReturnEmptyStream_whenPropertyIsEmptyString() {
        List<String> exceptedStreamAsList = Stream.of("").collect(Collectors.toList());
        Assertions.assertEquals(exceptedStreamAsList, resourceLoader.getFileAsStream("").collect(Collectors.toList()));
    }

    @Test
    void getFileAsStream_shouldReturnEmptyStream_whenPropertyIsNull() {
        List<String> exceptedStreamAsList = Stream.of("").collect(Collectors.toList());
        Assertions.assertEquals(exceptedStreamAsList, resourceLoader.getFileAsStream("").collect(Collectors.toList()));
    }

    @Test
    void getFileAsStream_shouldReturnEmptyStream_whenFileIsNotExist() {
        List<String> exceptedStreamAsList = Stream.of("").collect(Collectors.toList());
        Assertions.assertEquals(exceptedStreamAsList, resourceLoader.getFileAsStream("propertyWithoutData").collect(Collectors.toList()));
    }

    @Test
    void getFileAsStream_shouldReturnFileAsStream_whenFileAndPropertyAreExist() {
        List<String> exceptedStreamAsList = Stream.of("this", "is", "stream").collect(Collectors.toList());
        Assertions.assertEquals(exceptedStreamAsList, resourceLoader.getFileAsStream("data").collect(Collectors.toList()));
    }
}
